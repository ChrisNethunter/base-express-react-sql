
import React, { Component } from 'react';
import '../styles/App.css';

class Login extends Component {
	constructor(props) {
		super(props);
		this.state = {}
    }
    
	render() {
		return (
            <div className="App">
                <header className="App-header">
                    <img src="/logo.svg" className="App-logo" alt="logo" />
                    <p>
                       Login
                    </p>
                    <a
                        className="App-link"
                        href="https://reactjs.org"
                        target="_blank"
                        rel="noopener noreferrer"
                    >
                        Learn React
                    </a>
                </header>
            </div>
		);
	}
}

export default Login;
