import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

//Layouts
import MainLayout from './layouts/MainLayout';
import AdminLayout from './layouts/AdminLayout';
//Layouts

//Pages
import Home from './pages/Home.jsx';
import Login from './pages/Login.jsx';
import Dashboard from './pages/Dashboard.jsx';
import NotFound from './pages/NotFound.jsx';
//Pages

const AppRoute = ({ component:Component , layout:Layout ,...rest  } ) => (
    <Route {...rest} render={props=> (
        <Layout > <Component {...props}></Component></Layout>
    )}></Route>
);

export default class App extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <BrowserRouter basename="/">
                <Switch>
                    <AppRoute path="/" exact layout={MainLayout} component={() => <Home />} />
                    <AppRoute path="/login" exact layout={MainLayout} component={() => <Login />} />
                    <AppRoute path="/admin" exact layout={AdminLayout} component={() => <Dashboard />} />
                    <Route component={NotFound} />
                </Switch>
            </BrowserRouter>
        );
    }
}
