const db = require('../db/postgres');
const SchemaDB = require('../helpers/base_databse.js');
const {encrypt, decrypt} = require('../helpers/driver_encription.js')


// Users Method for control all the user database interaccions
/**
 * @class SessionChecker
 * @memberof SessionChecker
 *
 * @classdesc This class is in charge of generating consoles with colors to debug the most significant code
 *
 * @author  	Theevil24a theevil24a@gmail.com
 * @requires    colors require('colors')
 *
 */
class Users extends SchemaDB{
    constructor(parent) {
        super(parent);
        this.table_name = "Users"
        this.schema_db = `CREATE TABLE IF NOT EXISTS Users(
            ID TEXT PRIMARY KEY NOT NULL,
            NAME TEXT NOT NULL,
            EMAIL TEXT NOT NULL,
            PASSWORD TEXT NOT NULL,
            IV TEXT NOT NULL,
            CREATED_DATE TEXT NOT NULL
        );`

        this.primaty_key = "ID"
    }


    /**
     * @function insetWdthPassword
     *
     * @classdesc create a new user width password
     *
     * @author  	Theevil24a theevil24a@gmail.com
     * @requires    colors require('colors')
     *
     */
    insetWdthPassword (new_user) {
        let password = encrypt(String(new_user.PASSWORD))
        new_user['IV'] = password.iv
        new_user['PASSWORD'] = password.content
        this.InsetOne(new_user)
    }

    /**
     * @function validatePassword
     *
     * @classdesc validate password of user and generate token
     *
     * @author  	Theevil24a theevil24a@gmail.com
     * @requires    colors require('colors')
     *
     */
    validatePassword (username = false, password = false){
        return new Promise(resolve => {
            if (username && password){
                this.findEmail(username).then(response => {
                    if (response.length > 0){
                        let current = response[0]
                        let data = decrypt({iv: current.iv, content: current.password})
                        if (data){
                            return resolve(current)
                        }else{
                            return resolve({error: 'password_error'})
                        }
                    }else{
                        return resolve({error: 'user_not_exist_error'})
                    }
                })
            }else{
                return resolve({error: 'not_valid_parameters'})
            }
        })
    }

    /**
     * @function findEmail
     *
     * @classdesc to find a user per email
     *
     * @author  	Theevil24a theevil24a@gmail.com
     * @requires    colors require('colors')
     *
     */
    findEmail(email){
        return new Promise(resolve => {
            db.any(`SELECT * FROM ${this.table_name} WHERE EMAIL = '${email}';`, [true])
            .then(function(data) {
                resolve(data)
            })
            .catch(function(error) {
                resolve(error)
            });
        })
    }

    /* test_users(){
        let list_users = [{
            NAME : "User1",
            EMAIL : "user1@gmail.com",
            PASSWORD : "andres123",
            CREATED_DATE : new Date().getTime()
        },{
            NAME : "User2",
            EMAIL : "user2@gmail.com",
            PASSWORD : "andres123",
            CREATED_DATE : new Date().getTime()
        },{
            NAME : "User2",
            EMAIL : "user1@gmail.com",
            PASSWORD : "andres123",
            CREATED_DATE : new Date().getTime()
        }]

        for (let user of list_users){
            this.insetWdthPassword(user)
        }

        this.findAll().then(response => {
            console.log([response])
        })

    } */
}

module.exports = new Users();