const db = require('../db/postgres');
const SchemaDB = require('../helpers/base_databse.js');

/**
 * @class AuthUser
 * @extends SchemaDB
 *
 * @classdesc  AuthUser Method for control all the session of user
 *
 * @author  	Theevil24a theevil24a@gmail.com
 * @requires    colors require('colors')
 *
 */
class AuthUser extends SchemaDB{
    constructor(parent) {
        super(parent);
        this.table_name = "AuthUser"
        this.schema_db = `CREATE TABLE IF NOT EXISTS AuthUser(
            ID TEXT PRIMARY KEY NOT NULL,
            USER_ID TEXT NOT NULL,
            CONSTRAINT fk_user
                FOREIGN KEY(USER_ID) 
                    REFERENCES Users(ID),
            UUID TEXT NOT NULL,
            HASH TEXT NOT NULL,
            TOKEN TEXT NOT NULL
         );`
        this.primaty_key = "ID"
        this.CreateTable()
    }

    /**
     *  @function findOnePerTokenAndUser
     *
     * @classdesc for find the client per token and user id for validate exists
     *
     * @author  	Theevil24a theevil24a@gmail.com
     * @requires    colors require('colors')
     *
     */
    findOnePerTokenAndUser(user_id, token) {
        return new Promise(resolve => {
            db.any(`SELECT * FROM ${this.table_name} WHERE TOKEN = '${token}' AND USER_ID = '${user_id}' ;`, [true])
            .then(function(data) {
                resolve(data)
            })
            .catch(function(error) {
                resolve(error)
            });
        })
    }

    /**
     *  @function findOnePerToken
     *
     * @classdesc for get client if exist a session
     *
     * @author  	Theevil24a theevil24a@gmail.com
     * @requires    colors require('colors')
     *
     */
    findOnePerToken(token) {
        return new Promise(resolve => {
            db.any(`SELECT * FROM ${this.table_name} WHERE TOKEN = '${token}';`, [true])
            .then(function(data) {
                resolve(data)
            })
            .catch(function(error) {
                resolve(error)
            });
        })
    }

    /**
     *  @function DeleteOneLastSession
     *
     * @classdesc for delete the last session of user
     *
     * @author  	Theevil24a theevil24a@gmail.com
     * @requires    colors require('colors')
     *
     */
    DeleteOneLastSession(ID) {
        return new Promise(resolve => {
            db.any(`DELETE FROM ${this.table_name} WHERE USER_ID = '${ID}' ;`, [true])
            .then(function(data) {
                resolve(data)
            })
            .catch(function(error) {
                resolve(error)
            });
        })
    }

}

module.exports = new AuthUser();