const db = require('../db/postgres');
const randomstring = require("randomstring");


/**
 * @class SessionChecker
 * @memberof SessionChecker
 *
 * @classdesc This class is in charge of generating consoles with colors to debug the most significant code
 *
 * @author  	Theevil24a theevil24a@gmail.com
 * @requires    colors require('colors')
 *
 */
class SchemaDB {
    constructor() {
        this.table_name = ""
        this.schema_db = ""
        this.primaty_key = ""
        this.CreateTable()
    }

    /**
     * @function CreateTable
     *
     * @classdesc create table from class var
     *
     * @author  	Theevil24a theevil24a@gmail.com
     * @requires    colors require('colors')
     *
     */
    CreateTable() {
        return new Promise((resolve) => {
            db.any(this.schema_db, [true])
            .then(function(data) {
                resolve(data)
            })
            .catch(function(error) {
                resolve(error)
            });
        });
    }

    /**
     * @function findAll
     *
     * @classdesc find all the clients
     *
     * @author  	Theevil24a theevil24a@gmail.com
     * @requires    colors require('colors')
     *
     */
    findAll() {
        return new Promise(resolve => {
            db.any(`SELECT * FROM ${this.table_name};`, [true])
            .then(function(data) {
                resolve(data)
            })
            .catch(function(error) {
                resolve(error)
            });
        })
    }

    /**
     * @function findOne
     *
     * @classdesc find one client per id parameter
     *
     * @author  	Theevil24a theevil24a@gmail.com
     * @requires    colors require('colors')
     *
     */
    findOne(ID) {
        return new Promise(resolve => {
            db.any(`SELECT * FROM ${this.table_name} WHERE ${this.primaty_key} = '${ID}' ;`, [true])
            .then(function(data) {
                resolve(data)
            })
            .catch(function(error) {
                resolve(error)
            });
        })
    }

    /**
     * @function InsetOne
     *
     * @classdesc to insert and generate the id dinamic
     *
     * @author  	Theevil24a theevil24a@gmail.com
     * @requires    colors require('colors')
     *
     */
    InsetOne(object_to_insert) {
        return new Promise(resolve => {
            let keys = '';
            let values = '';

            let primaty_key = randomstring.generate(20)
            keys += ` ${this.primaty_key},`
            values += ` '${primaty_key}',`

            for (let item in object_to_insert){
                let is_last = Object.keys(object_to_insert)[Object.keys(object_to_insert).length -1] == item
                keys += ` ${item}${is_last?'':','}`
                values += ` '${object_to_insert[item]}'${is_last?'':','}`
            }

            db.any(`INSERT INTO ${this.table_name} (${keys}) VALUES (${values});`, [true])
            .then(function(data) {
                resolve(data)
            })
            .catch(function(error) {
                resolve(error)
            });
        })
    }

    /**
     * @function DeleteOne
     *
     * @classdesc delete from the primary key as parameter
     *
     * @author  	Theevil24a theevil24a@gmail.com
     * @requires    colors require('colors')
     *
     */
    DeleteOne(ID) {
        return new Promise(resolve => {
            db.any(`DELETE FROM ${this.table_name} WHERE ${this.primaty_key} = '${ID}' ;`, [true])
            .then(function(data) {
                resolve(data)
            })
            .catch(function(error) {
                resolve(error)
            });
        })
    }
}

module.exports = SchemaDB;