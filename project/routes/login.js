
const Users = require('../dao/users.js');
const AuthMiddleware = require('../middleware/AuthMiddleware.js')

// function for the text encription based private key of the settings
/**
 * @class Login
 * @memberof Login
 *
 * @classdesc this is the route for user post autentication
 *
 * @author  	Theevil24a theevil24a@gmail.com
 * @requires    colors require('colors')
 *
 */
class Login {
    constructor(){}
    async auth(req, res) {
        //?TEST: curl -X POST --data 'username=user1@gmail.com&password=andres123' http://127.0.0.1:8000/api/login/auth
        if ('username' in req.body && 'password' in req.body){
            let username = req.body.username
            let password = req.body.password
            
            let validate_password = await Users.validatePassword(username, password)
            if (validate_password){
                let current = validate_password
                AuthMiddleware.GenerateTokenSession(current.id).then(response => {
                    console.log({response})
                    return res.status(200).json(response);
                }).catch(response => {
                    return res.status(400).json(response);
                })
            }else{
                return res.status(400).json({error: 'auth-error'});
            }
        }else{
            return res.status(400).json({error: 'not-valid-parameters'});
        }

    }
}


module.exports = Login;

