const pgp = require("pg-promise")();
const CURRENT_ENV = process.env.NODE_ENV || 'development';
const config = require('../config.json');

var user = config[CURRENT_ENV].postgres.user;
var password = config[CURRENT_ENV].postgres.password;
var host = config[CURRENT_ENV].postgres.host;
var port = config[CURRENT_ENV].postgres.port;
var database = config[CURRENT_ENV].postgres.db;

let string = `postgres://${user}:${password}@${host}:${port}/${database}`;
const db = pgp(string);


/**
 * @class Database
 * @memberof pgp
 *
 * @classdesc  This class is responsible for making the connection to the database postgres
 *
 * @author  	ChrisNethunter cristian@vifuy.com
 * @requires    pg-promise require('pg-promise')
 *
 */

function connect (){
    db.connect()
    .then(obj => {
        // Can check the server version here (pg-promise v10.1.0+):
        const serverVersion = obj.client.serverVersion;
        obj.done(); // success, release the connection;
       
    })
    .catch(error => {
        console.log('ERROR:', error.message || error);
    });
}

connect();


module.exports = db
