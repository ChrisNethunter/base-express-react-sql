global.config = require('./project/config.json');
const express = require('express');
const compression = require('compression');
const helmet = require('helmet')
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser');
const path = require('path');
const app = express();
const server = require('http').createServer(app);
const port = global.config.port; 
const cors = require('cors');
const fs = require('fs');
const requestValidateApi = require('./project/middleware/requestValidateApi');
const sessionChecker = require('./project/middleware/validateSession');
const Logger = require('./project/helpers/logger.js');
const ENVIRONMENT = process.env.NODE_ENV || 'development';
const colors = require('colors');

var corsOptions = {
    origin: function (origin, callback) {
        callback(null, true);
    }
}

// initialize express-session to allow us track the logged-in user across sessions.
app.use(session({
    key: 'user_sid',
    secret: 'somerandonstuffs',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 600000
    }
}));


// Then pass them to cors:
app.use("*", cors(corsOptions));
app.disable('x-powered-by');
app.use( helmet({ contentSecurityPolicy: false }) );
app.set('view engine', 'pug')
app.set('trust proxy', 1) // trust first proxy
app.use(compression());
app.use(bodyParser.json()); // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({  extended: true })); // to support URL-encoded bodies
app.use(express.static(path.join(__dirname, 'build')));
app.use(cookieParser());// initialize cookie-parser to allow us access the cookies stored in the browser. 


/************************ Setting routes ************************/
var routes = {};
fs.readdirSync(__dirname + "/project/routes").forEach(function (file) {
    var moduleName = file.split('.')[0];
    routes[moduleName] = require(__dirname + '/project/routes/' + moduleName);
});
/************************ Setting routes ************************/

app.post('/api/:route/:method', requestValidateApi );
app.get('*',sessionChecker.validateSession, function (req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

server.listen(port, function (err) {
    Logger.info("App running on port " + port);
    Logger.info("ENVIRONMENT ".green + ENVIRONMENT );
   /*  test(); */
});



