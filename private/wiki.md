# INICIAR PROYECTO

    npm i 
    npm run dev

# REQUISITOS 

    ## PostgreSQL
    ## Pgadmin4
    ## Pm2

# INTALACION

## PostgreSQL

    https://www.postgresql.org/download/linux/ubuntu/

    ### Create the file repository configuration:
    sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

    # Import the repository signing key:
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

    # Update the package lists:
    sudo apt-get update

    # Install the latest version of PostgreSQL.
    # If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
    sudo apt-get -y install postgresql


    apt-get install pgadmin4

    Una vez instalado debemos asignar una contraseña al usuario postgres que por defecto no viene asignada 

    # sudo -u postgres psql
    # ALTER USER postgres PASSWORD 'micontrasena';