# Proyecto React + Express + Postgresql

Este proyecto base fue creado con [Create React App](https://github.com/facebook/create-react-app).

## Scripts

Comandos para iniciar el proyecto:

### `npm dev`

Inicia el proyecto en modo desarrollo.\
[http://localhost:3000](http://localhost:3000)

El proyecto se refresca automaticamente si cambiamos alguno de los archivos .\src

### `npm prod`

Inicia el proyecto en modo produccion.\
[http://localhost:3000](http://localhost:3000)

El proyecto se refresca automaticamente si cambiamos alguno de los archivos .\src

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Construye la aplicación para producción en la carpeta `build`. \
Agrupa correctamente React en el modo de producción y optimiza la compilación para obtener el mejor rendimiento.

La compilación se minimiza y los nombres de archivo incluyen los hash. \

[deployment](https://facebook.github.io/create-react-app/docs/deployment) 

### `npm run eject`

**Note: Esta es una operación unidireccional. Una vez usa el comando `eject`, no podra volver a atras!**

Si no está satisfecho con la herramienta de compilación y las opciones de configuración, puede "expulsar" en cualquier momento. Este comando eliminará la dependencia de compilación única de su proyecto.

En su lugar, copiará todos los archivos de configuración y las dependencias transitivas (paquete web, Babel, ESLint, etc.) directamente en su proyecto para que tenga un control total sobre ellos. Todos los comandos excepto `eject` seguirán funcionando, pero apuntarán a los scripts copiados para que pueda modificarlos. En este punto estás solo.

No es necesario que utilice nunca "eject". El conjunto de funciones seleccionadas es adecuado para implementaciones pequeñas y medianas, y no debe sentirse obligado a utilizar esta función. Sin embargo, entendemos que esta herramienta no sería útil si no pudiera persona

### `npm prod`

Inicia el proyecto en modo produccion.\
[http://localhost:3000](http://localhost:3000)

El proyecto se refresca automaticamente si cambiamos alguno de los archivos .\src

### `npm run deploy`

Este comando inicia el proyecto en modo produccion con pm2 con su ambiente de produccion ecosystem.config.js
[https://pm2.keymetrics.io] 

### `npm run restart_deploy`

Este comando reinicia el proyecto en modo produccion con pm2 
[https://pm2.keymetrics.io] 


### `npm run stop_deploy`

Este comando detiene el proyecto en modo produccion con pm2 
[https://pm2.keymetrics.io] 

### `npm run delete_deploy`

Este comando detiene el proyecto en modo produccion con pm2 
[https://pm2.keymetrics.io] 

### `npm run monitor_deploy`

Este comando abre el monitor de recursos del servidor con proyecto en modo produccion con pm2 
[https://pm2.keymetrics.io] 

### `npm run status_deploy`

Este comando nos reporta el estado del servidor con proyecto en modo produccion con pm2 
[https://pm2.keymetrics.io] 


### `npm run logs_deploy`

Este comando nos muestra los logs  del servidor en tiempo real con proyecto en modo produccion con pm2 
[https://pm2.keymetrics.io] 


### `npm run docs`

Este comando genera la documentacion del proyecto con jsdoc
[https://jsdoc.app] 

# INICIAR PROYECTO

    npm i 
    npm run dev

# REQUISITOS 

    ## PostgreSQL
    ## Pgadmin4
    ## Pm2

# INSTALACION

## PostgreSQL

    https://www.postgresql.org/download/linux/ubuntu/

    ### Create the file repository configuration:
    sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

    # Import the repository signing key:
    wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

    # Update the package lists:
    sudo apt-get update

    # Install the latest version of PostgreSQL.
    # If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
    sudo apt-get -y install postgresql


    apt-get install pgadmin4


    Una vez instalado debemos asignar una contraseña al usuario postgres que por defecto no viene asignada 

    # sudo -u postgres psql
    # ALTER USER postgres PASSWORD 'micontrasena';

